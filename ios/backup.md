Hi Apple Team!

emdocs is electron document turnover app for Agrobank(bank of Uzbekistan)

Please, before install E-Imzo(ID Karta) app(https://apps.apple.com/uz/app/e-imzo-id-%D0%BA%D0%B0%D1%80%D1%82%D0%B0/id1563416406) to use emdocs app. This app is Uzbekistan Government auth app and we need it.

Video instructions is here: https://gitlab.com/usmdroid/emdocs_handbook/-/blob/master/ios/video_instruction.mp4

All need sources(electron key and video instruction) are uploaded.

Electron key's(https://gitlab.com/usmdroid/emdocs_handbook/-/blob/master/ios/DS5555555560024.pfx) password - 123456

Login video instruction: https://gitlab.com/usmdroid/emdocs_handbook/-/blob/master/ios/login_video_instruction.mp4

Thanks!